const duration = 45; // Duration of the escape game in minutes


var express = require("express");
var app = express();
var http = require('http').Server(app);
var io = require("socket.io")(http);
var fs = require("fs");

var PORT = 80;

var msgs = [];
fs.readFile("messages.txt", 'utf8', function(err, buf) {
  if (err) throw err;
  msgs = buf.toString().trim().split("\n");
});

app.use("/", express.static("static"));

app.get("/", function(req, res){
  res.sendFile(__dirname + "/html/index.html");
});

app.get("/ctrl", function(req, res){
  res.sendFile(__dirname + "/html/controler.html");
});

io.on('connection', function(socket){
  console.log('an user connected');

  socket.emit('msgs', msgs);

  socket.on('say', function(msg){
    io.emit('send', msg);
  });

  socket.on('start', function(){
    stts = Date.now();
    it = setInterval(updateTimer, 100);
  });

  socket.on('stop', function(){
    try { clearInterval(it); } catch (e) {}
  });

  socket.on('ring', function(){
    io.emit('bell');
  });

  socket.on('clear', function(){
    io.emit('clr');
  });
});

function updateTimer(){
  perc = Math.min((Date.now() - stts)/1000/60/duration*100, 100);
  io.emit('time', perc);
}

http.listen(PORT, function(){
  console.log("Listening on port", PORT);
});
